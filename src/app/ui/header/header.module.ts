import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarModule } from '../navbar/navbar.module';
import { HeaderComponent } from './header.component';
import { NavbarComponent } from "../navbar/navbar.component";

@NgModule({
    declarations: [HeaderComponent],
    exports: [HeaderComponent],
    imports: [CommonModule, NavbarModule]
})
export class HeaderModule {}
